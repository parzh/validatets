import { check } from "checkts";
import { ValidationInput } from "./src/validation-input";
import { ValidationNumber } from "./src/validation-number";
import { ValidationString } from "./src/validation-string";
import { ValidationCommon } from "./src/validation-common";

export type common = number | string | object | symbol | boolean | void;

/**
 * Initiate validation chain for digital values only
 * @arg value The value to validate
 * @arg role (optional) Contextual role of the value. Helps error message to be more descriptive
 */
export default function validate(value: number, role?: string): ValidationNumber;

/**
 * Initiate validation chain for textual values only
 * @arg value The value to validate
 * @arg role (optional) Contextual role of the value. Helps error message to be more descriptive
 */
export default function validate(value: string, role?: string): ValidationString;

/**
 * Initiate validation chain
 * @arg value The value to validate
 * @arg role (optional) Contextual role of the value. Helps error message to be more descriptive
 */
export default function validate(value: common, role?: string): ValidationCommon;

export default function validate(value: any, role?: string): ValidationInput<any> {
	if (check.isNumber(value))
		return new ValidationNumber(value, role);

	else if (check.isString(value))
		return new ValidationString(value, role);

	else return new ValidationCommon(value, role);
}
