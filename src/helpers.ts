/**
 * Convert range to printable string
 * @arg range The range
 * @arg inclusively (optional) Whether the boundary match is counted. Default is `true`
 */
export function rangeToString(range: number[], inclusively: boolean = true): string {
	let _range = range.join(" to ");
	let brackets = inclusively? "[]" : "()";

	return brackets.split("").join(_range);
}

/**
 * Convert value to printable console-friendly string
 * @arg value The value
 */
export function valueToString(value: any): string {
	let type: string;

	if (value == null)
		type = "void";

	else if (typeof value === "object")
		if (value.constructor != null)
			type = (value.constructor as Function).name;

		else type = Object.name;

	else type = typeof value;

	return `<${ type }> ${ value }`;
}
