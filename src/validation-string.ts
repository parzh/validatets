import { check } from "checkts";
import { rangeToString } from "./helpers";
import { ValidationInput } from "./validation-input";
import { ValidationNumber } from "./validation-number";

export class ValidationString extends ValidationInput<string> {
	/**
	 * Throw error if the string does not start with the specified substring
	 * @arg substring Substring to compare against
	 * @arg position (optional) Specific position in the source string
	 */
	public startsWith(substring: string, position?: number): never | this {
		if (!check.startsWith(this.value, substring, position))
			return this.panic(`does not start with the specified substring "${ substring }"`);

		else return this;
	}

	/**
	 * Throw error if the string does not end with the specified substring
	 * @arg substring Substring to compare against
	 * @arg endPosition (optional) Specific position in the source string
	 */
	public endsWith(substring: string, endPosition?: number): never | this {
		if (!check.endsWith(this.value, substring, endPosition))
			return this.panic(`does not end with the specified substring "${ substring }"`);

		else return this;
	}

	/**
	 * Throw error if the string does not include the specified substring
	 * @arg substring Substring to compare against
	 * @arg position (optional) Specific position in the source string
	 */
	public includes(substring: string, position?: number): never | this {
		if (!check.includes(this.value, substring, position))
			return this.panic(`does not include the specified substring "${ substring }"`);

		else return this;
	}

	/**
	 * Throw error if the string does not match against the specified pattern
	 * @arg pattern Pattern to match against
	 */
	public matches(pattern: RegExp): never | this {
		if (!check.matches(this.value, pattern))
			return this.panic(`does not match against the specified pattern "${ pattern }"`);

		else return this;
	}

	/**
	 * Throw error if the string is longer or shorter than the specified length
	 * @arg length Specified length to compare against
	 */
	public length(length: number): never | this {
		new ValidationNumber(length, "string length").isNatural();

		if (this.value.length !== length)
			return this.panic(`is not ${ length } characters long`);

		else return this;
	}

	/**
	 * Throw error if the string is longer or shorter than the specified range of lengths
	 * @arg lengths Specified range of lengths to compare against
	 * @arg inclusively (optional) Whether to count boundary match. Default is `true`
	 */
	public lengthBetween(lengths: number[], inclusively: boolean = true): never | this {
		for (let length of lengths)
			new ValidationNumber(length, "string length").isNatural();

		let _lengths = rangeToString(lengths, inclusively);

		if (!check.isInRange(lengths, this.value.length, inclusively))
			return this.panic(`is not ${ _lengths } characters long`);

		else return this;
	}
}
