import { check } from "checkts";
import { panic } from "./panic";
import { ValidationNumber } from "./validation-number";
import { ValidationString } from "./validation-string";

export abstract class ValidationInput<T> {
	constructor(
		/** The value that's being validated */
		protected value: T,

		/** (optional) Contextual role of the value. Helps error message to be more descriptive */
		protected role?: string
	) { /* ... */ }

	// ***

	/**
	 * Throw error if the value is not a number
	 */
	public asNumber(): never | ValidationNumber {
		if (!check.isNumber(this.value))
			return this.panic("is not a number");

		else return new ValidationNumber(this.value);
	}

	/**
	 * Throw error if the value is not a string
	 */
	public asString(): never | ValidationString {
		if (!check.isString(this.value))
			return this.panic("is not a string");

		else return new ValidationString(this.value);
	}

	// ***

	/**
	 * @protected Throw error with the specified failure
	 * @arg failure Part of the message of the error to be thrown
	 */
	protected panic(failure: string): never {
		return panic(this.value, failure, this.role);
	}

	/**
	 * Throw error if the value is a void
	 */
	public isDefined(): never | this {
		return this.value == null? this.panic("is not defined") : this;
	}

	// TODO: validate.structure(descriptor: { [key: string]: any }, options?: { soft: boolean; voids: boolean; });
	/*
		`options.soft` means that properties unspecified in descriptor won't be validated in target object
		`options.voids` means that target object may have void values for specified properties
		`options.voids` may become `boolean | PropertyKey[]` in future
	 */

	/**
	 * Throw error if `thunk` with the value, placed as its first argument, returns `false`
	 * @arg thunk Callback that takes `ValidationInput.value` as the first argument and produces `boolean`
	 * @arg constraint (optional) Name or description of the constraint
	 * @example
	 * validate<number>(5, "the Value").custom(check.isDefined, "Not Void")
	 */
	public custom(thunk: (value?: T) => boolean, constraint?: string): never | this {
		let _constraint = constraint? `${ constraint } ` : "";

		if (!thunk(this.value))
			return this.panic(`has failed the ${ _constraint }constraint`);

		else return this;
	}

	/**
	 * Throw error if assertion is `false` or falsy
	 * @arg assertion Assertion
	 */
	public assert(assertion: boolean): never | this {
		return !assertion? panic("Assertion failed") : this;
	}
}
