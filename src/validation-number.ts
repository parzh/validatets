import { check } from "checkts";
import { ValidationInput } from "./validation-input";
import { rangeToString } from "./helpers";

export class ValidationNumber extends ValidationInput<number> {
	/**
	 * Throw error if the number is not finite number
	 */
	public isFinite(): never | this {
		if (!check.isFinite(this.value))
			return this.panic("is not finite");

		else return this;
	}

	/**
	 * Throw error if the number is not an integer
	 */
	public isInteger(): never | this {
		if (!check.isInteger(this.value))
			return this.panic("is not an integer");

		else return this;
	}

	/**
	 * Throw error if the number is negative
	 */
	public isNotNegative(): never | this {
		if (this.value < 0)
			return this.panic("is negative");

		else return this;
	}

	/**
	 * Throw error if the number is not natural
	 * @arg zero (optional) Whether to consider zero as a natural number. Default is `true`
	 */
	public isNatural(zero: boolean = true): never | this {
		if (!check.isNatural(this.value, zero))
			return this.panic("is not a natural number");

		else return this;
	}

	/**
	 * Throw error if the number is not in the provided range
	 * @arg range The range
	 * @arg inclusively (optional) Whether to count boundary match. Default is `true`
	 */
	public isInRange(range: number[], inclusively: boolean = true): never | this {
		let _range = rangeToString(range, inclusively);

		if (!check.isInRange(range, this.value, inclusively))
			return this.panic(`is not in the range ${ _range }`);

		else return this;
	}
}
