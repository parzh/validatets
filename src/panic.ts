import { check } from "checkts";
import { valueToString } from "./helpers";

export class ValidationError extends Error { /* ... */ }

/**
 * Throw error
 * @arg message Error message
 */
export function panic(message: string): never;

/**
 * Throw error
 * @arg value The failed value
 * @arg failure Description of the value failure
 * @arg role (optional) Contextual role of the value. Helps message to be more descriptive
 */
export function panic(value: any, failure: string, role?: string): never;

export function panic(value: any, failure?: string, role?: string): never {
	if (arguments.length === 1 && check.isString(value))
		throw new ValidationError(value);

	let _role = role? ` (${ role })` : "";

	throw new ValidationError(`The value${ _role } ${ failure }: ${ valueToString(value) }`);
}
